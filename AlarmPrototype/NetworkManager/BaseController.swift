//
//  BaseController.swift
//  Nihao
//
//  Created by Anurag Chauhan on 20/11/18.
//  Copyright © 2018 fluper. All rights reserved.
//

import UIKit

protocol APIResponseDelegate {
    func getAPiResponse(dicResponce : [String : Any],requestTitle : String)
}


class BaseController: UIViewController {

    
    
    
        var apiResponseDelegate : APIResponseDelegate?
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
        }
        
        func viewCornerRadiuslayout(_ view : UIView){
            if #available(iOS 11.0, *) {
                view.cornerRadius = 10
                view.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMinYCorner]
                
            }
        }
        
    
    func postToServerAPI(url : String,params : [String : Any],type : kHTTPMethod,completionHandler : @escaping (_ params : [String : Any])->Void) {
        
        
        print(url)
            CommonUtils.showHudWithNoInteraction(show: true)
            
            TANetworkManager.sharedInstance.requestApi(withServiceName: url,
                                                       requestMethod: type,
                                                       requestParameters: params ,
                                                       withProgressHUD: false)
            { (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
                
                CommonUtils.showHudWithNoInteraction(show: false)
                
                if errorType == .requestSuccess {
                    
                    let dictResult = kSharedInstance.getDictionary(result)
                    
                    switch Int.getInt(statusCode) {
                        
                    case 200:
            
                        print(dictResult)
                        completionHandler(dictResult)
                        
                        
                    default:
                       CommonUtils.showToast(message: String.getString(dictResult[kMessage]))
    
                    }
                    
                } else if errorType == .noNetwork {
                   
                    CommonUtils.showToast(message: AlertMessage.kNoInternet)
                    
                } else {
                    CommonUtils.showToast(message: AlertMessage.kDefaultError)
                }
            }
            
        }
    
    
    //MARK:- Multipart
    func postToServerRequestMultiPart(_ url : String, params : [String:Any],imageParams : [[String : Any]] ,completionHandler : @escaping (_ params : [String : Any])->Void) {
        
        CommonUtils.showHudWithNoInteraction(show: true)
        
        TANetworkManager.sharedInstance.requestMultiPart(withServiceName: url,
                                                         requestMethod: .post,
                                                         requestImages: imageParams,
                                                         requestVideos: [:],
                                                         requestData: params)
        {[weak self] (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
            
            CommonUtils.showHudWithNoInteraction(show: false)
            
            if errorType == .requestSuccess {
                
                let dictResult = kSharedInstance.getDictionary(result)
                
                switch Int.getInt(statusCode) {
                    
                case 200:
                    completionHandler(dictResult)
                default:
                    CommonUtils.showToast(message: String.getString(dictResult[kMessage]))
                }
                
            } else if errorType == .noNetwork {
                
                CommonUtils.showToast(message: AlertMessage.kNoInternet)
                
            } else {
                CommonUtils.showToast(message: AlertMessage.kDefaultError)
            }
        }
    }
}
