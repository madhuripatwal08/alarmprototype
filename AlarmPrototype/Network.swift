//
//  Network.swift
//  AlarmPrototype
//
//  Created by Mac on 09/01/22.
//

import Foundation
import UIKit

let baseURL = "https://us-central1-nuki-14d6e.cloudfunctions.net"
struct HttpUtility {
    static let shared = HttpUtility()
    private init(){}
    func getResult<T:Decodable>(requestUrl:String,resultType:T.Type,completionHandler: @escaping(_ result:T)->Void){
        URLSession.shared.dataTask(with: URL(string: "https://us-central1-nuki-14d6e.cloudfunctions.net/api/trackByDay/3")!) { (responseData, response, error) in
            if let error = error{
                print(error)
                return
            }
            let jsonDEcoder = JSONDecoder()
            do{
                let result =  try jsonDEcoder.decode(T.self, from: responseData!)
                completionHandler(result)
                print(result)
            }catch let error{
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func getBrowseFeaturePlaylist<T:Decodable>(requestType:T.Type,completionHandler:@escaping(_ result:T)->Void){
        
        var request = URLRequest(url: URL(string: "https://api.spotify.com/v1/playlists/37i9dQZEVXbMDoHDwVN2tF")!)
        request.addValue("Bearer \(UserDefaults.standard.value(forKey: "accessToken") ?? "")", forHTTPHeaderField: "Authorization")
      //  request.allHTTPHeaderFields = header
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with:request ) { (responseData, response, error) in
            if let error = error{
                
                return
                
            }
            let jsonDecoder = JSONDecoder()
            do{
              //  print(try JSONSerialization.data(withJSONObject: responseData!, options: .sortedKeys))
                let result = try jsonDecoder.decode(T.self, from: responseData!)
                print(result)
               completionHandler(result)
            }catch let error{
                print(error.localizedDescription)
            }
        }.resume()
    }
}

// MARK: - GlobalSongModel
struct GlobalSongModel: Codable {
    let href: String?
    let id: String?
    let name: String?
    let tracks: Tracks?
}


// MARK: - Followers
struct Followers: Codable {
    let href: String?
    let total: Int
}

// MARK: - Image
struct Images: Codable {
    let height: Int?
    let url: String
    let width: Int?
}

// MARK: - Owner
struct Owner: Codable {
    let displayName: String?
    let externalUrls: ExternalUrls
    let href: String
    let id: String
    let type: OwnerType
    let uri: String
    let name: String?

    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case externalUrls = "external_urls"
        case href, id, type, uri, name
    }
}

enum OwnerType: String, Codable {
    case artist = "artist"
    case user = "user"
}

// MARK: - Tracks
struct Tracks: Codable {
    let href: String?
    
    let items: [Items]?
    let limit: Int?
  
    let offset: Int?
 
    let total: Int?
}

// MARK: - Item
struct Items: Codable {
    var track: Trackss
}

// MARK: - Track
struct Trackss: Codable {
    let album: Albumss?
    let artists : [Artist]?
    let id: String?
    let name: String?
    let uri: String?
    let preview_url: String?
    var isPlaying : Bool?
    var isSelected : Bool?
}

// MARK: - Album
struct Albumss: Codable {
   

    let id: String?
    let images: [Images]?
    let name : String?
    let uri: String?

    
}





// MARK: - ExternalIDS
struct ExternalIDS: Codable {
    let isrc: String
}

enum TrackType: String, Codable {
    case track = "track"
}

// MARK: - VideoThumbnail
struct VideoThumbnail: Codable {
    let url: String?
}

struct ExternalUrls: Codable {
    let spotify: String
}


struct Artist : Codable {
    
        let externalUrls : ExternalUrls?
        let href : String?
        let id : String?
        let name : String?
        let type : String?
        let uri : String?

        enum CodingKeys: String, CodingKey {
                case externalUrls = "external_urls"
                case href = "href"
                case id = "id"
                case name = "name"
                case type = "type"
                case uri = "uri"
        }

}
