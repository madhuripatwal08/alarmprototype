//
//  SpotifyHelper.swift
//  AlarmPrototype
//
//  Created by Mac on 25/01/22.
//

import Foundation
open class SpotifyHelper: NSObject  {
    var categoryId : String = ""
    static let sharedInstance = SpotifyHelper()
    func fetchSpotifyAccessToken() {
        NetworkManager.fetchAccessToken { (result) in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                   print("Error fetching token", error.localizedDescription)
                }
            case .success(let spotifyAuth):
                print(spotifyAuth)
            }
        }
    }
//    func getArtists(artists : [Artist]) -> String {
//        var name : [String] = []
//        for value in artists
//        {
//            name.append(value.name ?? "")
//        }
//        return name.joined(separator: ",")
//    }
    func setCategoryId(catId : String)
    {
        categoryId  = catId
    }
    func getCategoryId() -> String
    {
        return categoryId
    }
}
