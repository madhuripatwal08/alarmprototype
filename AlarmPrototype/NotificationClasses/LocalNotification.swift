//
//  LocalNotification.swift
//  AlarmPrototype
//
//  Created by Mac on 12/01/22.
//

import Foundation
import UserNotifications

struct LocalNotificationManager {
    static let shared = LocalNotificationManager()
    
    func triggerNotificationAfterduration(){
        let content = UNMutableNotificationContent()
        content.body = "Wake Up"
        content.title = "My alarm"
        guard let audioName = UserDefaults.standard.value(forKey: "audioName") as? String else {return}
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: audioName))
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 118, repeats: false)
        content.categoryIdentifier = "Snooze"
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: UNNotificationActionOptions.init())
        let stopAction = UNNotificationAction(identifier: "Stop", title: "Stop", options: UNNotificationActionOptions.init())
        
        let actionCategory = UNNotificationCategory(identifier: "Snooze", actions: [snoozeAction,stopAction], intentIdentifiers: [], options: .customDismissAction)
        UNUserNotificationCenter.current().setNotificationCategories([actionCategory])
        
        let request = UNNotificationRequest(identifier: "Id2", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error{
                print(error)
            }else{
                print(request)
            }
        }
    }
    
    func triggerNotificationwithDateTime(date:Date,success:@escaping ()->()){
        let content = UNMutableNotificationContent()
        print("call ho gya ")
        content.title = "My alarm"
        content.body = "Wake up"
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "madhuriiii.caf"))
        content.categoryIdentifier = "Snooze"
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: UNNotificationActionOptions.init())
        let stopAction = UNNotificationAction(identifier: "Stop", title: "Stop", options: UNNotificationActionOptions.init())
        let actionCategory = UNNotificationCategory(identifier: "Snooze", actions: [snoozeAction,stopAction], intentIdentifiers: [], options: .customDismissAction)
        UNUserNotificationCenter.current().setNotificationCategories([actionCategory])
        
        let targetDate = date
        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year,.month,.minute,.second], from: targetDate), repeats: false)
        let request = UNNotificationRequest(identifier: "Ie", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error{
                print(error.localizedDescription)
            }else{
                print("successs")
                success()
            }
        }
        
    }
    
    func triggerNotificationwith(date:Date,success:@escaping ()->()){
        let content = UNMutableNotificationContent()
        content.title = "My alarm"
        content.body = "Wake up"
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "bell.mp3"))
        content.categoryIdentifier = "Snooze"
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: UNNotificationActionOptions.init())
        let stopAction = UNNotificationAction(identifier: "Stop", title: "Stop", options: UNNotificationActionOptions.init())
        
        let actionCategory = UNNotificationCategory(identifier: "Snooze", actions: [snoozeAction,stopAction], intentIdentifiers: [], options: .customDismissAction)
        UNUserNotificationCenter.current().setNotificationCategories([actionCategory])
        let targetDate = date
        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.second], from: targetDate), repeats: false)
        let request = UNNotificationRequest(identifier: "Id1", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error{
                print(error.localizedDescription)
            }else{
                success()
            }
        }
        
    }
}
