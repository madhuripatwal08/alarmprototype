//
//  AlarmVC.swift
//  AlarmPrototype
//
//  Created by Mac on 07/01/22.
//

import UIKit
import AVFoundation

class AlarmVC: UIViewController {
    
    var audioPlayer:AVAudioPlayer?
    
    @IBOutlet weak var datepicker: UIDatePicker!
    
    let userDefaults = UserDefaults()
    
    var completion : ((_ date:Date)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HttpUtility.shared.getBrowseFeaturePlaylist(requestType: GlobalSongModel.self, completionHandler: { [weak self] result in
          let response = result.tracks?.items?.map({$0.track}).filter({$0.preview_url != nil})
          let url =  response?.randomElement()
          print(url?.preview_url)
          print(url?.name)
          self?.downloadFileFromSpotify(url:url?.preview_url ?? "")
        })
    }
    
    @IBAction func setAlarm(_ sender: UIButton) {
        self.completion?(datepicker.date)
       // checkPathAndplaySound(link: "")
       // self.downloadFile1()
        userDefaults.setValue(datepicker.date, forKey: "date")
}
  
    func trimmSound( inUrl:URL, outUrl:URL,callBack:@escaping () -> Void){
                  
                  let startTime = CMTime(seconds: 0, preferredTimescale: 1000)
                  let duration = CMTime(seconds: 30, preferredTimescale: 1000)
                  let audioAsset = AVAsset(url: inUrl)
                  let composition = AVMutableComposition()
                  let compositionAudioTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID(kCMPersistentTrackID_Invalid))
        
                  let sourceAudioTrack = audioAsset.tracks(withMediaType: AVMediaType.audio).first!
                  do {
                       try compositionAudioTrack?.insertTimeRange(CMTimeRangeMake(start: startTime, duration: duration), of: sourceAudioTrack, at: .zero)
                       
                  } catch {
                    
                       print(error.localizedDescription)
                    
                       return
                  }
                  let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetPassthrough)
//        exportSession?.shouldOptimizeForNetworkUse = true
//        exportSession?.outputFileType = AVFileType.caf
                  exporter!.outputURL = outUrl
            
                  exporter!.outputFileType = AVFileType.caf
                  exporter!.shouldOptimizeForNetworkUse = true
                  exporter!.exportAsynchronously {
                       DispatchQueue.main.async {
                        print("££££££££\(outUrl)")
                            callBack()
                       }
                       
                  }
             }
        
    func downloadFileFromSpotify(url:String) {
            let fileManager = FileManager.default
            let documentPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
            let path = "\(documentPath[0])/Sounds"
            if !fileManager.fileExists(atPath: path) {
                try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            } else {
                print("Directroy is already created")

            }
        //https://open.spotify.com/playlist/37i9dQZF1DX0UrRvztWcAU//ExternalUrl
        //https://p.scdn.co/mp3-preview/29a694f8fe68bc6543afebecb9be5ca544265b75?cid=a40af69083de41a3b55f737cf2909ae7"

        //https://www.kozco.com/tech/LRMonoPhase4.mp3//madhurii
       // https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3//madhuri
        //https://www.soundhelix.com/examples/mp3/SoundHelix-Song-11.mp3//madhuriii
       // UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            if let audioUrl = URL(string: url) {
                
                // then lets create your document folder url
                let libraryDirectoryURL =  FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
                
                // lets create your destination file url
                
                let soundPathUrl = libraryDirectoryURL.appendingPathComponent("Sounds/")
                
                let destinationUrl = soundPathUrl.appendingPathComponent(audioUrl.lastPathComponent+".mp3")
                print(destinationUrl)
                
                // to check if it exists before downloading it
                if FileManager.default.fileExists(atPath: destinationUrl.path) {
                    print("The file already exists at path")
                    
                    // if the file doesn't exist
                } else {
                    
                    // you can use NSURLSession.sharedSession to download the data asynchronously
                    URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                        guard let location = location, error == nil else { return }
                        do {
                            // after downloading your file you need to move it to your destination url
                            try FileManager.default.moveItem(at: location, to: destinationUrl)
                            print("File moved to documents folder")
                            let outUrl: URL = URL(string: "\(soundPathUrl.absoluteURL)" + "/"+audioUrl.lastPathComponent+".caf")!
                            print(audioUrl.lastPathComponent)
                            UserDefaults.standard.setValue(audioUrl.lastPathComponent+".caf", forKey: "audioName")
                            self.trimmSound(inUrl: destinationUrl, outUrl: outUrl) {
                            }
                            
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                    }).resume()
                }
            }
            
        }
    
    func downloadFile(withUrl url: URL, andFilePath filePath: URL, completion: @escaping ((_ filePath: URL)->Void)){
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data.init(contentsOf: url)
                try data.write(to: filePath, options: .atomic)
                print("saved at \(filePath.absoluteString)")
                DispatchQueue.main.async {
                    completion(filePath)
                    print(filePath)
                }
            } catch {
                print("an error happened while downloading or saving the file")
            }
        }
    }
    
    func play(url: URL) {
        print("playing \(url)")

        do {

            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.prepareToPlay()
            audioPlayer?.delegate = self
            audioPlayer?.play()
            let percentage = (audioPlayer?.currentTime ?? 0)/(audioPlayer?.duration ?? 0)
            DispatchQueue.main.async {
                // do what ever you want with that "percentage"
            }

        } catch let error {
            audioPlayer = nil
        }

    }
}
extension AlarmVC:AVAudioPlayerDelegate{
    func playDownloaded(){
    if let audioUrl = URL(string: "http://freetone.org/ring/stan/iPhone_5-Alarm.mp3") {
               
               // then lets create your document folder url
               let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
               
               // lets create your destination file url
               let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
    
           //let url = Bundle.main.url(forResource: destinationUrl, withExtension: "mp3")!
           
           do {
               audioPlayer = try AVAudioPlayer(contentsOf: destinationUrl)
               guard let player = audioPlayer else { return }
               
               player.prepareToPlay()
               player.play()
           } catch let error {
               print(error.localizedDescription)
           }
           }
           
       }

}
