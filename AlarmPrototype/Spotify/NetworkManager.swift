//
//  NetworkManager.swift
//  BSocial
//
//  Created by User on 18/08/21.
//

import Foundation
import CryptoKit
import CommonCrypto

class NetworkManager {
    //make it singleton
    public static let shared = NetworkManager()
    private init() {}
    //properties
    static let urlSession = URLSession.shared // shared singleton session object used to run tasks. Will be useful later
    static private let baseURL = "https://accounts.spotify.com/"
    static private var parameters: [String: String] = [:]
    static let clientId = "a40af69083de41a3b55f737cf2909ae7"
    static let clientSecretKey = "e775d73aa5504d0da4de540a286b7fec"
    static let redirectUri = URL(string:"alarmPrototype://callback")!
    static private let defaults = UserDefaults.standard
    static  var codeVerifier: String = ""
    static var authorizationCode = defaults.string(forKey: Constants.authorizationCodeKey) {
        didSet { defaults.set(authorizationCode, forKey: Constants.authorizationCodeKey) }
        
    }

   
    
    //MARK: Static Methods

    ///fetch accessToken from Spotify
    static func fetchAccessToken(completion: @escaping (Result<SpotifyAuth, Error>) -> Void) {
       // guard let code = authorizationCode else { return completion(.failure(EndPointError.missing(message: "No authorization code found."))) }
        codeVerifier =   challenge(verifier1: "SecretPassword")
        let url = URL(string: "\(baseURL)api/token")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let authorizationValue = "Basic \((clientId + ":" + clientSecretKey).data(using: .utf8)!.base64EncodedString())"
        request.allHTTPHeaderFields = ["Authorization": authorizationValue,
                                       "Content-Type": "application/x-www-form-urlencoded"]
        var requestBodyComponents = URLComponents()
//        let scopeAsString = stringScopes.joined(separator: " ") //put array to string separated by whitespace
        requestBodyComponents.queryItems = [
            //URLQueryItem(name: "client_id", value: clientId),
            URLQueryItem(name: "grant_type", value: "client_credentials"),
            //URLQueryItem(name: "code", value: code),
           // URLQueryItem(name: "redirect_uri", value: redirectUri.absoluteString),
         //   URLQueryItem(name: "code_verifier", value: codeVerifier),
       //     URLQueryItem(name: "scope", value: scopeAsString),
        ]
        request.httpBody = requestBodyComponents.query?.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,                              // is there data
                  let response = response as? HTTPURLResponse,  // is there HTTP response
                  (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                  error == nil else {                           // was there no error, otherwise ...
                return completion(.failure(EndPointError.noData(message: "No data found")))
            }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase  //convert keys from snake case to camel case
            do {
                if let spotifyAuth = try? decoder.decode(SpotifyAuth.self, from: data) {
//                    self.accessToken = spotifyAuth.accessToken
//                    self.spotifyAuth = spotifyAuth
                    SpotifyAuth.setCurrent(spotifyAuth, writeToUserDefaults: true)
                    self.authorizationCode = nil
//                    self.refreshToken = spotifyAuth.refreshToken
                    UserDefaults.standard.set(spotifyAuth.accessToken, forKey: "accessToken")
                    UserDefaults.standard.set("1", forKey:"status")
                    return completion(.success(spotifyAuth))
                }
                completion(.failure(EndPointError.couldNotParse(message: "Failed to decode data")))
            }
        }
        task.resume()
    }
    
    static func refreshAcessToken(completion: @escaping (Result<SpotifyAuth, Error>) -> Void) {
        guard let refreshToken = SpotifyAuth.current?.refreshToken else { return completion(.failure(EndPointError.missing(message: "No refresh token found."))) }
        let url = URL(string: "\(baseURL)api/token")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let spotifyAuthKey = "Basic \((clientId + ":" + clientSecretKey).data(using: .utf8)!.base64EncodedString())"
        request.allHTTPHeaderFields = ["Authorization": spotifyAuthKey,
                                       "Content-Type": "application/x-www-form-urlencoded"]
        var requestBodyComponents = URLComponents()
        requestBodyComponents.queryItems = [
            URLQueryItem(name: "grant_type", value: "refresh_token"),
            URLQueryItem(name: "refresh_token", value: refreshToken),
            URLQueryItem(name: "client_id", value: clientId),
        ]
        request.httpBody = requestBodyComponents.query?.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,                              // is there data
                  let response = response as? HTTPURLResponse,  // is there HTTP response
                  (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                  error == nil else {                           // was there no error, otherwise ...
                return completion(.failure(EndPointError.noData(message: "No data found")))
            }
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase  //convert keys from snake case to camel case
            do {
//                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any]
//                print(jsonResult)
                if let spotifyAuth = try? decoder.decode(SpotifyAuth.self, from: data) {
                    //update access token
//                    self.accessToken = spotifyAuth.accessToken
//                    self.spotifyAuth?.accessToken = spotifyAuth.accessToken
                    guard var spotifyAuthToUpdate = SpotifyAuth.current else { return }
                    spotifyAuthToUpdate.accessToken = spotifyAuth.accessToken
                    spotifyAuthToUpdate.expiresIn = spotifyAuth.expiresIn
                    spotifyAuthToUpdate.scope = spotifyAuth.scope
                    SpotifyAuth.setCurrent(spotifyAuthToUpdate, writeToUserDefaults: true)
               
                    print("Refreshed Access Token: \(spotifyAuth.accessToken)")
                    return completion(.success(spotifyAuth))
                }
                completion(.failure(EndPointError.couldNotParse(message: "Failed to decode data")))
            }
        }
        task.resume()
    }
    
    //MARK: Helpers
    
    ///create a code verifier that meets Spotify's requirement in order to fetch code and access token
 static   func setCodeVerifier() {
        guard let data = "SecretPassword".data(using: .utf8) else { return }
        let digest = SHA256.hash(data: data)
        let digestString = digest.map { String(format: "%02X", $0) }.joined()
        let codeChallengeMethod = Data(digestString.utf8).base64EncodedString()
        codeVerifier = codeChallengeMethod
    }
    
    
    static  func challenge(verifier1: String) -> String {
        
        
        var buffer = [UInt8](repeating: 0, count: 32)
        _ = SecRandomCopyBytes(kSecRandomDefault, buffer.count, &buffer)
        let verifier = Data(bytes: buffer).base64EncodedString()
            .replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "__")
            .replacingOccurrences(of: "=", with: "")
            .trimmingCharacters(in: .whitespaces)
        
        guard let verifierData = verifier.data(using: String.Encoding.utf8) else { return "error" }
            var buffer1 = [UInt8](repeating: 0, count:Int(CC_SHA256_DIGEST_LENGTH))
     
            verifierData.withUnsafeBytes {
                CC_SHA256($0.baseAddress, CC_LONG(verifierData.count), &buffer1)
            }
        let hash = Data(_: buffer1)
        print(hash)
        let challenge = hash.base64EncodedData()
        return String(decoding: challenge, as: UTF8.self)
            .replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
            .trimmingCharacters(in: .whitespaces)
    }
}
