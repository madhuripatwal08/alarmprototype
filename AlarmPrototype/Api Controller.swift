//
//  AppDelegate.swift
//  AlarmPrototype
//
//  Created by Mac on 07/01/22.
//

import Foundation

let ApiInstance = Apicontroller.shared_instance
import Alamofire

class Apicontroller {
    //MARK;- Shared Instance
    static let shared_instance = Apicontroller()

    //func For Single Part Api
    func postApi(parameters :Dictionary<String ,Any> ,serviceName: String, completionClosure: @escaping (_ result: Any?) -> ()) -> Void{
        AF.re
        AlmofireApiInstanse.requestApi(withServiceName: serviceName, requestMethod: .POST, requestParameters: parameters, withProgressHUD: false)
        {[weak self] (result: Any?, error: Error?, errorType: ErrorType, statusCode: Int?) in
            CommonUtils.showHudWithNoInteraction(show: false)
            guard self != nil else { return }
            if errorType == ErrorType.requestSuccess {
                let dicResponse     = kSharedInstance.getDictionary(result)
                switch Int.getInt(statusCode) {
                case 200:
                    let data = dicResponse[kResponse]
                    completionClosure(data)
                case 501:
                    showAlertMessage.alert(message: AlertMessage.Under_Development)
                default:
                    showAlertMessage.alert(message: "\(String.getString(dicResponse[ApiParameters.message])) 🎈")
                    break;
                }
            }
            else {
                showAlertMessage.alert(message: AlertMessage.knoNetwork)
            }
        }
    }
    
}


