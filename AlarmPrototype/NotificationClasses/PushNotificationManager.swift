//
//  PushNotificationManager.swift
//
//
//  Created by Macbookro
//  Copyright © 2022.
//    All rights reserved.
//



import UIKit
import AVFoundation
import UserNotifications
import FirebaseMessaging


class PushNotificationManager: NSObject {
    
    static let shared = PushNotificationManager()
    
    override init() {
        super.init()
    }
    
    var audioPlayer: AVAudioPlayer?
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            //  UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert,.sound]
            
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.sound,.alert], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
    }
}

//MARK:-FCM Messaging Delegate
//============================
extension PushNotificationManager:MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        print("FCM\(fcmToken!)")
        
    }
}

extension PushNotificationManager: UNUserNotificationCenterDelegate{
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print(error.localizedDescription)
    }
    
    //AlarmApplicationDelegate protocol
    //================================
    func playSound(_ soundName: String) {
        
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        //set vibrate callback
        AudioServicesAddSystemSoundCompletion(SystemSoundID(kSystemSoundID_Vibrate),nil,
                                              nil,
                                              { (_:SystemSoundID, _:UnsafeMutableRawPointer?) -> Void in
                                                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                                              },
                                              nil)
        let url = URL(fileURLWithPath: Bundle.main.path(forResource: soundName, ofType: "mp3")!)
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        if let err = error {
            print("audioPlayer error \(err.localizedDescription)")
            return
        } else {
            //audioPlayer!.delegate = self
            audioPlayer!.prepareToPlay()
        }
        
        audioPlayer!.numberOfLoops = -1
        audioPlayer!.play()
    }
    
}
