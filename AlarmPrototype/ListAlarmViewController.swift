//
//  ListAlarmViewController.swift
//  AlarmPrototype
//
//  Created by Mac on 09/01/22.
//

import UIKit
import AVFoundation
import Alamofire

class ListAlarmViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    var myAlarm = [Alarmm]()
    var player = AVPlayer()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
       
    }

    @IBAction func addAlarmAction(_ sender: UIBarButtonItem) {
        
        if #available(iOS 13.0, *) {
            guard let alarmScene = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "AlarmVC") as? AlarmVC else {return}
            alarmScene.completion = {[weak self] date in
                let newAlarm = Alarmm(date: date, id: "10")
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                self?.myAlarm.append(newAlarm)
                let content = UNMutableNotificationContent()
                content.title = "My New alarm"
                content.body = "Wake up"
                guard let audioName = UserDefaults.standard.value(forKey: "audioName") as? String else {return}
                content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: audioName))
                content.categoryIdentifier = "Snooze"
                let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: UNNotificationActionOptions.init())
                let stopAction = UNNotificationAction(identifier: "Stop", title: "Stop", options: UNNotificationActionOptions.init())
                
                let actionCategory = UNNotificationCategory(identifier: "Snooze", actions: [snoozeAction,stopAction], intentIdentifiers: [], options: .customDismissAction)
                UNUserNotificationCenter.current().setNotificationCategories([actionCategory])
                
                let targetDate = date
                let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: targetDate), repeats: false)
                let request = UNNotificationRequest(identifier: "Ie", content: content, trigger: trigger)
                UNUserNotificationCenter.current().add(request) { (error) in
                    if let error = error{
                        print(error.localizedDescription)
                    }else{
                        print("successs")
                    }
                }
//                LocalNotificationManager.shared.triggerNotificationwithDateTime(date: date) {
//                    DispatchQueue.main.async {
                        self?.tableView.reloadData()
//                    }
//
//                }
                self?.navigationController?.popViewController(animated: true)
          }
            self.navigationController?.pushViewController(alarmScene, animated: true)
        } else {
            // Fallback on earlier versions
        }
      
       
        
    }
    @IBAction func selectSongAction(_ sender: UIBarButtonItem) {
        guard  let playListScene = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "PlaylistVC") as? PlaylistVC else {return}
        self.navigationController?.present(playListScene, animated: true, completion: nil)
    }
    
    @IBAction func fireNotification(){
       // HttpUtility.shared.getResult(requestUrl: "", resultType: Response.self) {[weak self] (result) in
 //           print(result)
//            if !result.data.url.isEmpty{
//                self?.checkBookFileExists(withLink: result.data.url){ [weak self] downloadedURL in
//                        guard let self = self else{
//                            return
//                        }
//                    self.play(url: downloadedURL as NSURL)
//                    }
//                }
//
//            self?.play(url: NSURL(string: result.data.url)!)
//        }
//        UNUserNotificationCenter.current().requestAuthorization(options: [.sound,.alert,.badge]) {[weak self] success,error in
//            if success{
//                self?.triggerAlarm()
//            }
//            else if let error = error{
//                print(error.localizedDescription)
//            }
//        }
//
    }
    
    func play(url:NSURL) {
        print("playing \(url)")
            let playerItem = AVPlayerItem(url: url as URL)
            self.player = AVPlayer(playerItem:playerItem)
            player.volume = 1.0
            player.play()
    }
 
    func checkBookFileExists(withLink link: String, completion: @escaping ((_ filePath: URL)->Void)){
        let urlString = link.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url  = URL.init(string: urlString ?? ""){
            let fileManager = FileManager.default
            if let documentDirectory = try? fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create: false){

                let filePath = documentDirectory.appendingPathComponent(url.lastPathComponent, isDirectory: false)

                do {
                    if try filePath.checkResourceIsReachable() {
                        print("file exist")
                        completion(filePath)

                    } else {
                        print("file doesnt exist")
                        downloadFile(withUrl: url.absoluteURL, andFilePath: filePath, completion: completion)
                    }
                } catch {
                    print("file doesnt exist")
                    downloadFile(withUrl: url.absoluteURL, andFilePath: filePath, completion: completion)
                }
            }else{
                 print("file doesnt exist")
            }
        }else{
                print("file doesnt exist")
        }
    }

    func downloadFile(withUrl url: URL, andFilePath filePath: URL, completion: @escaping ((_ filePath: URL)->Void)){
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data.init(contentsOf: url)
                try data.write(to: filePath, options: .atomic)
                print("saved at \(filePath.absoluteString)")
                DispatchQueue.main.async {
                    completion(filePath)
                }
            } catch {
                print("an error happened while downloading or saving the file")
            }
        }
    }
}

extension ListAlarmViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myAlarm.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlarmCell", for: indexPath) as? AlarmCell else {fatalError("cell not found")}
        let date = myAlarm[indexPath.row].date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d,h:mm a"
        cell.dateTextLabel.text = dateFormatter.string(from: date)
        
        return cell
        
        
    }
    
}
//MARK-AV
extension ListAlarmViewController:AVAudioPlayerDelegate{
    private func hitApi(){
        
    }



    
}
class AlarmCell:UITableViewCell{
    @IBOutlet weak var dateTextLabel: UILabel!
    
}

struct Alarmm {
    var date : Date
    var id :String
    
    
}

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}
