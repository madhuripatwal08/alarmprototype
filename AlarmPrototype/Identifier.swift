//
//  AppDelegate.swift
//  AlarmPrototype
//
//  Created by Mac on 07/01/22.
//

import Foundation

struct Id {
    static let stopIdentifier = "Alarm-ios-swift-stop"
    static let snoozeIdentifier = "Alarm-ios-swift-snooze"
//    static let addSegueIdentifier = "addSegue"
//    static let editSegueIdentifier = "editSegue"
//    static let saveSegueIdentifier = "saveEditSegue"
//    static let soundSegueIdentifier = "soundSegue"
//    static let labelSegueIdentifier = "labelEditSegue"
//    static let weekdaysSegueIdentifier = "weekdaysSegue"
//    static let settingIdentifier = "setting"
//    static let musicIdentifier = "musicIdentifier"
//    static let alarmCellIdentifier = "alarmCell"
//    
//    static let labelUnwindIdentifier = "labelUnwindSegue"
//    static let soundUnwindIdentifier = "soundUnwindSegue"
//    static let weekdaysUnwindIdentifier = "weekdaysUnwindSegue"
}

protocol AlarmApplicationDelegate {
    func playSound(_ soundName: String)
}
struct Constants {
    static let spotifyAuthKey = ""  //From developer account auth key
    static let authorizationCodeKey = ""
    static let currentUser = ""
    
}
//
//
//// MARK: - GlobalSongModel
//struct GlobalSongModel: Codable {
//    let href: String?
//    let id: String?
//    let name: String?
//    var tracks: Tracks?
//}
//// MARK: - Followers
//struct Followers: Codable {
//    let href: String?
//    let total: Int
//}
//// MARK: - Image
//struct Images: Codable {
//    let height: Int?
//    let url: String
//    let width: Int?
//}
//// MARK: - Owner
//struct Owner: Codable {
//    let displayName: String?
//    let externalUrls: ExternalUrls
//    let href: String
//    let id: String
//    let type: OwnerType
//    let uri: String
//    let name: String?
//    enum CodingKeys: String, CodingKey {
//        case displayName = "display_name"
//        case externalUrls = "external_urls"
//        case href, id, type, uri, name
//    }
//}
//enum OwnerType: String, Codable {
//    case artist = "artist"
//    case user = "user"
//}
//// MARK: - Tracks
//struct Tracks: Codable {
//    let href: String?
//    var items: [Items]?
//    let limit: Int?
//    let offset: Int?
//    let total: Int?
//}
//// MARK: - Item
//struct Items: Codable {
//    var track: Trackss
//}
//// MARK: - Track
//struct Trackss: Codable {
//    let album: Albumss?
//    let artists : [Artist]?
//    let id: String?
//    let name: String?
//    let uri: String?
//    let preview_url: String?
//    var isPlaying : Bool?
//    var isSelected : Bool?
//}
//// MARK: - Album
//struct Albumss: Codable {
//    let id: String?
//    let images: [Images]?
//    let name : String?
//    let uri: String?
//}
//// MARK: - ExternalIDS
//struct ExternalIDS: Codable {
//    let isrc: String
//}
//enum TrackType: String, Codable {
//    case track = "track"
//}
//// MARK: - VideoThumbnail
//struct VideoThumbnail: Codable {
//    let url: String?
//}
//struct ExternalUrls: Codable {
//    let spotify: String
//}
//struct Artist : Codable {
//        let externalUrls : ExternalUrls?
//        let href : String?
//        let id : String?
//        let name : String?
//        let type : String?
//        let uri : String?
//        enum CodingKeys: String, CodingKey {
//                case externalUrls = "external_urls"
//                case href = "href"
//                case id = "id"
//                case name = "name"
//                case type = "type"
//                case uri = "uri"
//        }
//}
