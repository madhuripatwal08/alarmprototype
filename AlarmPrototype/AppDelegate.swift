//
//  AppDelegate.swift
//  AlarmPrototype
//
//  Created by Mac on 07/01/22.
//

import UIKit
import AVFoundation
//import CoreData
import FirebaseCore

@main
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        PushNotificationManager.shared.registerForPushNotifications()
        UNUserNotificationCenter.current().delegate = self
        SpotifyHelper().fetchSpotifyAccessToken()

        return true
    }
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       //  self.pushNotifications.handleNotification(userInfo: userInfo)
        UserDefaults.standard.setValue("HarshiiiiiSirupppp1pp2p3pp4", forKey: "Yes")
     
        print("UserInfo\(userInfo)")
        downloadFile1()
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("sdfghjkl")
        // Decide how the notification (that was received in the foreground)
        // should be presented to the user
    }
    
  
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
           print("Snooze")
           let identifier = response.actionIdentifier
           if identifier == "Snooze"{
            LocalNotificationManager.shared.triggerNotificationAfterduration()
                
           }
           if identifier == "stop"{
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
           }
           completionHandler()
       }

//    func addNotification(content:UNNotificationContent,trigger:UNNotificationTrigger?, indentifier:String){
//            let request = UNNotificationRequest(identifier: indentifier, content: content, trigger: trigger)
//            UNUserNotificationCenter.current().add(request, withCompletionHandler: {
//                (errorObject) in
//                if let error = errorObject{
//                    print("Error \(error.localizedDescription) in notification \(indentifier)")
//                }
//            })
//        }
//
    func trimmSound( inUrl:URL, outUrl:URL,callBack:@escaping () -> Void){
                   
                   let startTime = CMTime(seconds: 0, preferredTimescale: 1000)
                   let duration = CMTime(seconds: 30, preferredTimescale: 1000)
                   let audioAsset = AVAsset(url: inUrl)
                   let composition = AVMutableComposition()
                   let compositionAudioTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID(kCMPersistentTrackID_Invalid))
                   
                   let sourceAudioTrack = audioAsset.tracks(withMediaType: AVMediaType.audio).first!
                   do {
                        try compositionAudioTrack?.insertTimeRange(CMTimeRangeMake(start: startTime, duration: duration), of: sourceAudioTrack, at: .zero)
                        
                   } catch {
                        print(error.localizedDescription)
                        return
                   }
                   let exporter = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetPassthrough)
 //        exportSession?.shouldOptimizeForNetworkUse = true
 //        exportSession?.outputFileType = AVFileType.caf
                   exporter!.outputURL = outUrl
             
                   exporter!.outputFileType = AVFileType.caf
                   exporter!.shouldOptimizeForNetworkUse = true
                   exporter!.exportAsynchronously {
                        DispatchQueue.main.async {
                         print("££££££££\(outUrl)")
                             callBack()
                        }
                        
                   }
              }
         
      func downloadFile1() {
             let fileManager = FileManager.default
             let documentPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
             let path = "\(documentPath[0])/Sounds"
             if !fileManager.fileExists(atPath: path) {
                 try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
             } else {
                 print("Directroy is already created")

             }
        // UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
             if let audioUrl = URL(string: "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba.mp3") {
                 
                 // then lets create your document folder url
                 let libraryDirectoryURL =  FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
                 
                 // lets create your destination file url
                 
                 let soundPathUrl = libraryDirectoryURL.appendingPathComponent("Sounds/")
                 
                 let destinationUrl = soundPathUrl.appendingPathComponent(audioUrl.lastPathComponent)
                 print(destinationUrl)
                 
                 // to check if it exists before downloading it
                 if FileManager.default.fileExists(atPath: destinationUrl.path) {
                     print("The file already exists at path")
                     
                     // if the file doesn't exist
                 } else {
                     
                     // you can use NSURLSession.sharedSession to download the data asynchronously
                     URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                         guard let location = location, error == nil else { return }
                         do {
                             // after downloading your file you need to move it to your destination url
                             try FileManager.default.moveItem(at: location, to: destinationUrl)
                             print("File moved to documents folder")
                             let outUrl: URL = URL(string: "\(soundPathUrl.absoluteURL)" + "/madhuri.caf")!
                                                   
                             self.trimmSound(inUrl: destinationUrl, outUrl: outUrl) {
                                                    
                                                       print("")
                             }
                             
                         } catch let error as NSError {
                             print(error.localizedDescription)
                         }
                     }).resume()
                 }
             }
             
         }
}
