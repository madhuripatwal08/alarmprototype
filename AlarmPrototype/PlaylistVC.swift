//
//  PlaylistVC.swift
//  AlarmPrototype
//
//  Created by Mac on 25/01/22.
//

import UIKit
import DropDown

class PlaylistVC: UIViewController {
//
//    @IBOutlet weak var artistDropDown: DropDown!
    @IBOutlet weak var artistTextField: UITextField!
    @IBOutlet weak var genereTextField: UITextField!
    @IBOutlet weak var tracksTextField: UITextField!
    let dropDown = DropDown()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
    }
    
    @IBAction func artistBtnAction(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.dataSource = ["Mohit Chauhan","Arman Malik","Victoria Linnen","AP Dillon","Kevin Chambers"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.artistTextField.text = item
        
        }
        dropDown.show()
        
    }
    @IBAction func genereBtnAction(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.dataSource = ["genere 1","genere 2","genere 3","genere 4","genere 5"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.genereTextField.text = item

        }
        dropDown.show()
        
    }
    
    @IBAction func tracksBtnAction(_ sender: UIButton) {
        let dropDown = DropDown()
        dropDown.anchorView = sender // UIView or UIBarButtonItem
        dropDown.dataSource = ["Track 1","Track 2","Track 3","Track 4","Track 5"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tracksTextField.text = item
            
        }
        dropDown.show()
        
    }


}

//MARK:-PRIVATE FUNCTIONS
//=======================
extension PlaylistVC{
    private func initialSetUp(){
        HttpUtility.shared.getBrowseFeaturePlaylist(requestType: GlobalSongModel.self, completionHandler: {[weak self] result in
        let response =    result.tracks?.items?.map({$0.track}).filter({$0.preview_url != nil})
            print(response ?? "")
        })
    }
    
}
